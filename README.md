Senior Project for School of Information Technology, King Mongkut's University of Technology (2018)

Under supervision of
- Asst. Prof. Dr. Chakarida Nukoolkit
- Assoc. Prof. Dr. Jonathan Hoyin Chan

Our members:
- Jukrapop Kongkaew
- Tin Krutsakon
- Patcharapol Thanasuttawong

Scopes:
- Virtual Reality Game
- Standalone Game
- Using Unreal Engine 4
- Encourage players to move/exercise while playing
- Report estimated benefits

Gameplay:
- Accuracy - to hit the ojects corresponding to the sabers' colors to score.
- Quick Math - to hit the correct answer to the given equation to score.

Game design:
- Sets of blocks (targets) come in forms that encourage player to swing their arm and move their body.
- Powerups are the elements of entertainment.
- Currently has 2 levels of difficulty.

![titlescene](img/1.png "Title Scene")
![titlescene2](img/3.png "Title Scene Overview")
![casualgameplay](img/4.png "Casual Gameplay")
![hardgameplay](img/5.png "Hard Gameplay")
![quickmathgameplay](img/2.png "Quick Math Gameplay")
![summary](img/9.png "Match Summary")
![activity](img/14.png "Activity Board")
![scoreboard](img/15.png "Scoreboard")
